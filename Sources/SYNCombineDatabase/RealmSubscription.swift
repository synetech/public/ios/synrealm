//
//  RealmSubscription.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import Combine
import RealmSwift

final class RealmSubscription<Output, Failure: Error>: Subscription {

    // MARK: - Aliases
    typealias Handler = (AnySubscriber<Output, Failure>) -> NotificationToken?

    // MARK: - Properties
    private let handler: Handler
    private let initialValue: Output?

    private var subscriber: AnySubscriber<Output, Failure>?
    private var token: NotificationToken?
    
    // MARK: - Init
    init<S: Subscriber>(
        subscriber: S,
        initialValue: Output?,
        handler: @escaping Handler
    ) where Failure == S.Failure, Output == S.Input {
        self.subscriber = AnySubscriber(subscriber)
        self.initialValue = initialValue
        self.handler = handler
    }
}

// MARK: - Subscription and disposal handling
extension RealmSubscription {

    func request(_ demand: Subscribers.Demand) {
        if let subscriber = subscriber, token == nil {
            token = handler(subscriber)
            if let initialValue = initialValue {
                _ = subscriber.receive(initialValue)
            }
        }
    }

    func cancel() {
        token?.invalidate()
        subscriber = nil
    }
}
