//
//  CombineDatabase.swift
//  
//
//  Created by Tom Novotny on 15.04.2021.
//

import Combine
import Foundation
import RealmSwift

public protocol CombineDatabase {

    // MARK: - Add
    func add<T: Object>(_ object: T) -> AnyPublisher<Any, Error>
    func addAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>

    // MARK: - Update
    func update<T: Object>(_ object: T, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
    func update<T: Object, V>(_ object: T,
                              changedProperty: ReferenceWritableKeyPath<T, V>,
                              value: V) -> AnyPublisher<Any, Error>
    func update<T: Object, U>(_ type: T.Type, id: U, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
    func update<T: Object, U, V>(_ type: T.Type, id: U,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> AnyPublisher<Any, Error>
    func updateAll<T: Object>(_ objects: [T], updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
    func updateAll<T: Object, V>(_ objects: [T],
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> AnyPublisher<Any, Error>
    func updateAll<T: Object>(_ type: T.Type, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
    func updateAll<T: Object, V>(_ type: T.Type,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> AnyPublisher<Any, Error>

    // MARK: - Update with predicate
    func updateAllWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate,
                                           updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
    func updateAllWithPredicate<T: Object, V>(_ type: T.Type, predicate: NSPredicate,
                                              changedProperty: ReferenceWritableKeyPath<T, V>,
                                              value: V) -> AnyPublisher<Any, Error>

    // MARK: - Get
    func get<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error>
    func getOnce<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error>
    func getFirst<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error>
    func getFirstOnce<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error>
    func getAll<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error>
    func getAllOnce<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error>

    // MARK: - Get with predicate
    func getFirst<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<T, Error>
    func getFirstOnce<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<T, Error>
    func getWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Results<T>, Error>
    func getOnceWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Results<T>, Error>

    // MARK: - Delete
    func delete<T: Object>(_ object: T) -> AnyPublisher<Any, Error>
    func delete<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<Any, Error>
    func deleteAll<T: Object>(_ type: T.Type) -> AnyPublisher<Any, Error>
    func deleteAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>
    func deleteWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Any, Error>
    func deleteCurrentAndAddAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>

    func deleteDatabase()
}
