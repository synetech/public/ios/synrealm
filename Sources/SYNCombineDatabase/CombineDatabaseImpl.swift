//
//  CombineDatabaseImpl.swift
//  
//
//  Created by Tom Novotny on 15.04.2021.
//

import Combine
import Foundation
import RealmSwift

public class CombineDatabaseImpl: CombineDatabase {

    typealias T = Object
    
    // MARK: Enum
    public enum DatabaseError: Error {
        // TODO: Extend
        case unsupportedIdType
        case objectNotFound
        case objectWasInvalidated
    }
    
    public typealias MigrationBlock = RealmSwift.MigrationBlock?
    typealias Configuration = Realm.Configuration
    
    typealias Operation = (Realm) -> Void
    typealias ErrorCallback = (Error) -> Void
    typealias NoParameterCallback = () -> Void
    
    // MARK: - Properties
    private let databaseSchemaVersion: UInt64
    private let migrationBlock: MigrationBlock
    private let inMemoryIdentifier: String?

    private var config: Configuration {
        var config = Configuration()

        config.schemaVersion = databaseSchemaVersion
        config.migrationBlock = migrationBlock
        config.deleteRealmIfMigrationNeeded = migrationBlock == nil
        if let inMemoryIdentifier = inMemoryIdentifier {
            config.inMemoryIdentifier = inMemoryIdentifier
        }

        #if DEBUG
        if let fileLocation = config.fileURL?.absoluteString {
            print("[DEBUG] Realm location is " + fileLocation)
        } else {
            print("File not found")
        }
        #endif
        return config
    }

    // MARK: - Init
    public init(databaseSchemaVersion: UInt64, migrationBlock: MigrationBlock = nil, inMemoryIdentifier: String? = nil) {
        self.databaseSchemaVersion = databaseSchemaVersion
        self.migrationBlock = migrationBlock
        self.inMemoryIdentifier = inMemoryIdentifier
    }
}

// MARK: - Add
public extension CombineDatabaseImpl {
    
    func add<T: Object>(_ object: T) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in realm.add(object, update: .modified) })
    }
    
    func addAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in realm.add(objects, update: .modified) })
    }
}

// MARK: - Update
public extension CombineDatabaseImpl {
    
    func update<T: Object>(_ object: T, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { _ in updateBlock(object) })
    }
    
    func update<T: Object, V>(_ object: T, changedProperty: ReferenceWritableKeyPath<T, V>, value: V) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { _ in object[keyPath: changedProperty] = value })
    }
    
    func updateAll<T: Object>(_ objects: [T], updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { _ in objects.forEach(updateBlock) })
    }
    
    func updateAll<T: Object, V>(_ objects: [T],
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { _ in objects.forEach { $0[keyPath: changedProperty] = value } })
    }
    
    func updateAll<T: Object>(_ type: T.Type, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in realm.objects(type).forEach(updateBlock) })
    }
    
    func updateAll<T: Object, V>(_ type: T.Type,
                                 changedProperty: ReferenceWritableKeyPath<T, V>,
                                 value: V) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in
            realm.objects(type).forEach { $0[keyPath: changedProperty] = value }
        })
    }

    func update<T, U>(_ type: T.Type, id: U, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error> where T : Object {
        if isIdValid(id) {
            return Deferred {
                Future<Any, Error> { promise in
                    self.writeOperation(operation: { realm in
                        if let object = realm.object(ofType: T.self, forPrimaryKey: id) {
                            updateBlock(object)
                            promise(.success(0))
                        } else {
                            promise(.failure(DatabaseError.objectNotFound))
                        }
                    }, onError: { error in
                        promise(.failure(error))
                    })
                }
            }
            .eraseToAnyPublisher()
        } else {
            return Result<Any, Error>.Publisher(.failure(DatabaseError.unsupportedIdType))
                .eraseToAnyPublisher()

        }
    }
    
    func update<T, U, V>(_ type: T.Type, id: U, changedProperty: ReferenceWritableKeyPath<T, V>, value: V) -> AnyPublisher<Any, Error> where T : Object {
        return update(type, id: id, updateBlock: { $0[keyPath: changedProperty] = value })
    }
}

// MARK: - Update with predicate
public extension CombineDatabaseImpl {
    
    func updateAllWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate,
                                           updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in
            realm.objects(type).filter(predicate).forEach(updateBlock)
        })
    }
    
    func updateAllWithPredicate<T: Object, V>(_ type: T.Type, predicate: NSPredicate,
                                              changedProperty: ReferenceWritableKeyPath<T, V>,
                                              value: V) -> AnyPublisher<Any, Error> {
        return simpleOperation(operation: { realm in
            realm.objects(type).filter(predicate).forEach { $0[keyPath: changedProperty] = value }
        })
    }
}

// MARK: - Get
public extension CombineDatabaseImpl {
    
    func get<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error> {
        if isIdValid(id) {
            return getOperation(getObjectOperation: { realm in realm.object(ofType: T.self, forPrimaryKey: id) })
        } else {
            return Result<T, Error>.Publisher(.failure(DatabaseError.unsupportedIdType))
                .eraseToAnyPublisher()
        }
    }
    
    func getOnce<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error> {
        if isIdValid(id) {
            return getOperation(getObjectOperation: { realm in realm.object(ofType: T.self, forPrimaryKey: id) })
                .prefix(1)
                .eraseToAnyPublisher()
        } else {
            return Result<T, Error>.Publisher(.failure(DatabaseError.unsupportedIdType))
                .eraseToAnyPublisher()
        }
    }
    
    func getFirst<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).first })
    }
    
    func getFirstOnce<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).first })
            .prefix(1)
            .eraseToAnyPublisher()
    }
    
    func getAll<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(T.self) })
    }
    
    
    func getAllOnce<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type) })
            .prefix(1)
            .eraseToAnyPublisher()
    }
}

// MARK: - Get with predicate
public extension CombineDatabaseImpl {
    
    func getFirst<T: Object>(_ type: T.Type,
                             predicate: NSPredicate) -> AnyPublisher<T, Error> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).filter(predicate).first })
    }
    
    func getFirstOnce<T: Object>(_ type: T.Type,
                                 predicate: NSPredicate) -> AnyPublisher<T, Error> {
        return getOperation(getObjectOperation: { realm in realm.objects(type).filter(predicate).first })
            .prefix(1)
            .eraseToAnyPublisher()
    }
    
    func getWithPredicate<T: Object>(_ type: T.Type,
                                     predicate: NSPredicate) -> AnyPublisher<Results<T>, Error> {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type).filter(predicate) })
    }

    func getOnceWithPredicate<T>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Results<T>, Error> where T : Object {
        return getAllOperation(getObjectsOperation: { realm in realm.objects(type).filter(predicate) })
            .prefix(1)
            .eraseToAnyPublisher()
    }
}

// MARK: - Delete
public extension CombineDatabaseImpl {

     func delete<T>(_ object: T) -> AnyPublisher<Any, Error> where T : Object {
        return simpleOperation(operation: { realm in realm.delete(object) })
    }

    func delete<T, U>(_ type: T.Type, id: U) -> AnyPublisher<Any, Error> where T : Object {
        if isIdValid(id) {
            return Deferred {
                Future<Any, Error> { promise in
                    self.writeOperation(operation: { realm in
                        if let object = realm.object(ofType: T.self, forPrimaryKey: id) {
                            realm.delete(object)
                            promise(.success(0))
                        } else {
                            promise(.failure(DatabaseError.objectNotFound))
                        }
                    }, onError: { error in
                        promise(.failure(error))
                    })
                }
            }
            .eraseToAnyPublisher()
        } else {
            return Result<Any, Error>.Publisher(DatabaseError.unsupportedIdType)
                .eraseToAnyPublisher()
        }
    }

    func deleteAll<T>(_ type: T.Type) -> AnyPublisher<Any, Error> where T : Object {
        return simpleOperation(operation: { realm in
            let objects = realm.objects(type)
            realm.delete(objects)
        })
    }
    
    func deleteAll<T>(_ objects: [T]) -> AnyPublisher<Any, Error> where T : Object {
        return simpleOperation(operation: { realm in realm.delete(objects) })
    }
    
    func deleteWithPredicate<T>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Any, Error> where T : Object {
        return simpleOperation(operation: { realm in
            let objects = realm.objects(type).filter(predicate)
            realm.delete(objects)
        })
    }

    func deleteCurrentAndAddAll<T>(_ objects: [T]) -> AnyPublisher<Any, Error> where T : Object {
        return simpleOperation(operation: { realm in
            let objectsToDelete = realm.objects(T.self)
            realm.delete(objectsToDelete)
            realm.add(objects, update: .modified)
        })
    }

    func deleteDatabase() {
        let realm = try? Realm(configuration: config)
        try? realm?.write {
          realm?.deleteAll()
        }
    }
}

// MARK: - Operations
private extension CombineDatabaseImpl {

    func simpleOperation(operation: @escaping Operation) -> AnyPublisher<Any, Error> {
        Deferred { [weak self] in
            Future<Any, Error> { promise in
                self?.writeOperation(operation: { realm in
                    operation(realm)
                    promise(.success(0))
                }, onError: { error in
                    promise(.failure(error))
                })
            }
        }
        .eraseToAnyPublisher()
    }

    func getOperation<T: Object>(getObjectOperation: @escaping (Realm) -> T?) -> AnyPublisher<T, Error> {
        RealmPublisher<T, Error>() { subscriber in
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                if let object = getObjectOperation(realm) {
                    _ = subscriber.receive(object)
                    token = object.observe({ (_) in
                        if object.isInvalidated {
                            subscriber.receive(completion: .failure(DatabaseError.objectWasInvalidated))
                        } else {
                            _ = subscriber.receive(object)
                        }
                    })
                } else {
                    subscriber.receive(completion: .failure(DatabaseError.objectNotFound))
                }
            }, onError: { error in
                subscriber.receive(completion: .failure(error))
            })
            return token
        }
        .eraseToAnyPublisher()
    }

    func getAllOperation<T: Object>(getObjectsOperation: @escaping (Realm) -> Results<T>) -> AnyPublisher<Results<T>, Error> {
        RealmPublisher<Results<T>, Error>() { subscriber in
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                let objects = getObjectsOperation(realm)
                _ = subscriber.receive(objects)
                token = objects.observe { changeset in
                    switch changeset {
                    case.initial:
                        break
                    case .update(let values, _, _, _):
                        _ = subscriber.receive(values)
                    case.error(let error):
                        subscriber.receive(completion: .failure(error))
                    }
                }
            }, onError: { error in
                subscriber.receive(completion: .failure(error))
            })
            return token
        }
        .eraseToAnyPublisher()
    }
    
    func writeOperation(operation: @escaping Operation,
                        onError: ErrorCallback,
                        onCompleted: NoParameterCallback? = nil) {
        do {
            let realm = try Realm(configuration: config)
            
            try write(operation: operation, to: realm)
            onCompleted?()
        } catch {
            handleError(error, errorCallback: onError)
        }
    }
    
    func readOperation(operation: Operation, onError: ErrorCallback) {
        do {
            let realm = try Realm(configuration: config)
            operation(realm)
        } catch {
            handleError(error, errorCallback: onError)
        }
    }
}

// MARK: - Private
private extension CombineDatabaseImpl {
    
    func write(operation: Operation, to realm: Realm) throws {
        if realm.isInWriteTransaction {
            operation(realm)
            try realm.commitWrite()
        } else {
            try realm.write {
                operation(realm)
                try realm.commitWrite()
            }
        }
    }
    
    func handleError(_ error: Error, errorCallback: ErrorCallback) {
        print("FATAL: Error while interacting with Realm!\n\(error)")
        errorCallback(error)
    }
    
    func isIdValid<U>(_ id: U) -> Bool {
        return id is String || id is Int || id is Bool || id is NSDate
    }
}
