//
//  RealmPublisher.swift
//  
//
//  Created by Tom Novotny on 15.04.2021.
//

import Combine
import Foundation
import RealmSwift

struct RealmPublisher<Output, Failure: Error>: Publisher {

    // MARK: - Aliases
    typealias Handler = (AnySubscriber<Output, Failure>) -> NotificationToken?

    // MARK: - Properties
    private let handler: Handler
    private let initialValue: Output?

    // MARK: - Init
    init(initialValue: Output? = nil, handler: @escaping Handler) {
        self.handler = handler
        self.initialValue = initialValue
    }
}

// MARK: - Subscription binding
extension RealmPublisher {

    func receive<S: Subscriber>(subscriber: S) where S.Failure == Failure, S.Input == Output {
        subscriber.receive(
            subscription: RealmSubscription<Output, Failure>(
                subscriber: subscriber, initialValue: initialValue, handler: handler
            )
        )
    }
}
