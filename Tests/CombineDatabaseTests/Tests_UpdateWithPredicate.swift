//
//  Tests_UpdateWithPredicate.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions:
///
/// func updateAllWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate,
///                                 updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
/// func updateAllWithPredicate<T: Object, V>(_ type: T.Type, predicate: NSPredicate,
///                                    changedProperty: ReferenceWritableKeyPath<T, V>,
///                                    value: V) -> AnyPublisher<Any, Error>
///
class Tests_UpdateWithPredicate: CombineDatabaseTests {

    func test_UpdateAllWithPredicate_WithBlock() {
        addTestData(TestDBEntity(id: testEntityID))

        combineRealm.get(TestDBEntity.self, id: testEntityID)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { result in
                    XCTAssertEqual(result.content, self.testContent)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.updateAllWithPredicate(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID),
            updateBlock: { (entity: TestDBEntity) in
                entity.content = self.testContent
            }
        )
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_UpdateAllWithPredicate() {
        addTestData(TestDBEntity(id: testEntityID))

        combineRealm.get(TestDBEntity.self, id: testEntityID)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { result in
                    XCTAssertEqual(result.content, self.testContent)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.updateAllWithPredicate(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID),
            changedProperty: \.content,
            value: testContent
        )
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
