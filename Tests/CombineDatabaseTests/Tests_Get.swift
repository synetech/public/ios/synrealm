//
//  Tests_Get.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions
///
/// func get<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error>
/// func getOnce<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<T, Error>
/// func getFirst<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error>
/// func getFirstOnce<T: Object>(_ type: T.Type) -> AnyPublisher<T, Error>
/// func getAll<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error>
/// func getAllOnce<T: Object>(_ type: T.Type) -> AnyPublisher<Results<T>, Error>
///
class Tests_Get: CombineDatabaseTests {

    func test_Get() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.get(TestDBEntity.self, id: testEntityID)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { result in
                    XCTAssertEqual(result.id, self.testEntityID)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetOnce() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.getOnce(TestDBEntity.self, id: testEntityID)
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { result in
                    XCTAssertEqual(result.id, self.testEntityID)
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetFirst() {
        let entities = createEntities(count: 1)
        let id = entities[0].id
        
        combineRealm.getAllOnce(TestDBEntity.self)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                }
            )
            .store(in: &disposeBag)
        
        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)
        
        combineRealm.getFirst(TestDBEntity.self)
            .dropFirst() // To check if update was received
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { result in
                    XCTAssert(result.id == id)
                    XCTAssert(result.content == self.testContent)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.updateAll(TestDBEntity.self) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetFirstOnce() {
        let entities = createEntities(count: 1)
        let id = entities[0].id

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)
        
        combineRealm.getFirstOnce(TestDBEntity.self)
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { result in
                    print("result \(result)")
                    XCTAssert(result.id == id)
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetAll() {
        let entities = createEntities(count: 1)
        let id = entities[0].id
        
        combineRealm.getAllOnce(TestDBEntity.self)
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                }
            )
            .store(in: &disposeBag)
        
        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)
        
        combineRealm.getFirstOnce(TestDBEntity.self)
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { result in
                    XCTAssert(result.id == id)
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetAllOnce() {
        combineRealm.getAllOnce(TestDBEntity.self)
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
