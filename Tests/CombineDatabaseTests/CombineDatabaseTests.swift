//
//  CombineDatabaseTests.swift
//  
//
//  Created by Vojtěch Pajer on 11/11/2019.
//

@testable import SYNCombineDatabase

import Combine
import RealmSwift
import XCTest

class CombineDatabaseTests: XCTestCase {

    // MARK: - Properties
    let combineRealm: CombineDatabase = CombineDatabaseImpl(databaseSchemaVersion: 1)
    let testEntityID = "1234"
    let testContent = "Test content"
    let expectationMessage = "Waiting for results"

    var expectation: XCTestExpectation!
    var disposeBag = Set<AnyCancellable>()

    // MARK: - Setup
    override func setUp() {
        super.setUp()
        expectation = XCTestExpectation(description: expectationMessage)
    }

    // MARK: - Tear down
    override func tearDown() {
        super.tearDown()
        combineRealm.deleteAll(TestDBEntity.self)
            .sinkAndStore(in: &disposeBag)
        disposeBag = Set<AnyCancellable>()
    }
}

// MARK: - Helper functions
extension CombineDatabaseTests {

    func addTestData(_ entity: TestDBEntity? = nil, count: Int = 1) {
        if count > 1 {
            let entities = createEntities(count: count)
            combineRealm.addAll(entities)
                .sinkAndStore(in: &disposeBag)
        } else {
            combineRealm.add(entity ?? TestDBEntity())
                .sinkAndStore(in: &disposeBag)
        }
    }

    func createEntities(count: Int) -> [TestDBEntity] {
        return (0..<count).map { _ in TestDBEntity() }
    }
}
