//
//  TestDBEntity.swift
//  
//
//  Created by Tom Novotny on 15.04.2021.
//

import Foundation
import RealmSwift

class TestDBEntity: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var content: String = ""

    convenience init(id: String) {
        self.init()
        self.id = id
    }

    override class func primaryKey() -> String? { "id" }
}
