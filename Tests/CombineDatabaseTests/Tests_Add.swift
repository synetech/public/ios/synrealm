//
//  Tests_Add.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions:
///
/// func add<T: Object>(_ object: T) -> AnyPublisher<Any, Error>
/// func addAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>
///
class Tests_Add: CombineDatabaseTests {

    func test_Add() {
        let newData = TestDBEntity(id: testEntityID)

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertEqual(results.first?.id, self.testEntityID)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.add(newData)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_AddAll() {
        let testCount = 10

        let entities = (0..<testCount).map { _ in TestDBEntity() }

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertEqual(results.count, testCount)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
