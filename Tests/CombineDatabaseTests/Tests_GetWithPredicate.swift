//
//  Tests_GetWithPredicate.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions
///
/// func getFirst<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<T, Error>
/// func getFirstOnce<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<T, Error>
/// func getWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Results<T>, Error>
/// func getOnceWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Results<T>, Error>
///
class Tests_GetWithPredicate: CombineDatabaseTests {

    func test_GetFirst() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.getFirst(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID)
        )
        .dropFirst()
        .sink(
            receiveCompletion: { _ in
                XCTFail("This stream should not complete")
            },
            receiveValue: { result in
                XCTAssertEqual(result.content, self.testContent)
                self.expectation.fulfill()
            }
        )
        .store(in: &disposeBag)

        combineRealm.updateAll(TestDBEntity.self) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)
        
        wait(for: [expectation], timeout: 1)
    }

    func test_GetFirstOnce() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.getFirstOnce(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID)
        )
            .sink(
                receiveCompletion: { _ in
                    self.expectation.fulfill()
                },
                receiveValue: { result in
                    XCTAssertEqual(result.id, self.testEntityID)
                }
            )
            .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetWithPredicate() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.getWithPredicate(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID)
        )
        .dropFirst()
        .sink(
            receiveCompletion: { _ in },
            receiveValue: { result in
                XCTAssertEqual(result.first?.id, self.testEntityID)
                XCTAssertEqual(result.first?.content, self.testContent)
                self.expectation.fulfill()
            }
        )
        .store(in: &disposeBag)

        combineRealm.updateAll(TestDBEntity.self) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_GetOnceWithPredicate() {
        let entities = createEntities(count: 3)
        entities[1].id = testEntityID

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.getOnceWithPredicate(
            TestDBEntity.self,
            predicate: NSPredicate(format: "id == %@", testEntityID)
        )
        .sink(
            receiveCompletion: { _ in
                self.expectation.fulfill()
            },
            receiveValue: { results in
                XCTAssertEqual(results.first?.id, self.testEntityID)
            }
        )
        .store(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
