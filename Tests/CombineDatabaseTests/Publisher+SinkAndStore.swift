//
//  Publisher+SinkAndStore.swift
//  
//
//  Created by Tom Novotny on 15.04.2021.
//

import XCTest
import Combine

extension Publisher {

    func sinkAndStore(in set: inout Set<AnyCancellable>) {
        sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &set)
    }
}
