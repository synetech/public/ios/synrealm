//
//  Tests_Update.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions:
///
/// func update<T: Object>(_ object: T, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
/// func update<T: Object, V>(_ object: T,
///                        changedProperty: ReferenceWritableKeyPath<T, V>,
///                        value: V) -> AnyPublisher<Any, Error>
/// func update<T: Object, U>(_ type: T.Type, id: U, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
/// func update<T: Object, U, V>(_ type: T.Type, id: U,
///                           changedProperty: ReferenceWritableKeyPath<T, V>,
///                           value: V) -> AnyPublisher<Any, Error>
/// func updateAll<T: Object>(_ objects: [T], updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
/// func updateAll<T: Object, V>(_ objects: [T],
///                          changedProperty: ReferenceWritableKeyPath<T, V>,
///                          value: V) -> AnyPublisher<Any, Error>
/// func updateAll<T: Object>(_ type: T.Type, updateBlock: @escaping (T) -> Void) -> AnyPublisher<Any, Error>
/// func updateAll<T: Object, V>(_ type: T.Type,
///                          changedProperty: ReferenceWritableKeyPath<T, V>,
///                          value: V) -> AnyPublisher<Any, Error>
///
class Tests_Update: CombineDatabaseTests {

    func test_Update_Object_WithBlock() {
        let testData = TestDBEntity()

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertEqual(results.first?.content, self.testContent)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(testData)

        combineRealm.update(testData) { (data: TestDBEntity) in
            data.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_Update_Object() {
        let testData = TestDBEntity()

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertEqual(results.first?.content, self.testContent)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(testData)

        combineRealm.update(testData, changedProperty: \.content, value: testContent)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_Update_ByType_WithBlock() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(count: 2)

        combineRealm.updateAll(TestDBEntity.self) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_Update_ByType() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(count: 2)

        combineRealm.updateAll(TestDBEntity.self, changedProperty: \.content, value: testContent)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_UpdateAll_Object_WithBlock() {
        let entities = createEntities(count: 5)
        
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.updateAll(entities) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_UpdateAll_Object() {
        let entities = createEntities(count: 5)
        
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.updateAll(entities, changedProperty: \.content, value: testContent)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_UpdateAll_ByType_WithBlock() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(count: 2)

        combineRealm.updateAll(TestDBEntity.self) { (entity: TestDBEntity) in
            entity.content = self.testContent
        }
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_UpdateAll_ByType() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    results.forEach { result in
                        XCTAssertEqual(result.content, self.testContent)
                    }
                        self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)
        
        addTestData(count: 2)

        combineRealm.updateAll(TestDBEntity.self, changedProperty: \.content, value: testContent)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
