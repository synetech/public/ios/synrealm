//
//  Tests_Delete.swift
//  
//
//  Created by Tom Novotny on 16.04.2021.
//

import XCTest

///
/// Protocol functions
/// 
/// func delete<T: Object>(_ object: T) -> AnyPublisher<Any, Error>
/// func delete<T: Object, U>(_ type: T.Type, id: U) -> AnyPublisher<Any, Error>
/// func deleteAll<T: Object>(_ type: T.Type) -> AnyPublisher<Any, Error>
/// func deleteAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>
/// func deleteWithPredicate<T: Object>(_ type: T.Type, predicate: NSPredicate) -> AnyPublisher<Any, Error>
/// func deleteCurrentAndAddAll<T: Object>(_ objects: [T]) -> AnyPublisher<Any, Error>
///
/// func deleteDatabase()
///
class Tests_Delete: CombineDatabaseTests {

    func test_Delete() {
        let testData = TestDBEntity()
        addTestData(testData)

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.delete(testData)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_DeleteWithID() {
        let testData = TestDBEntity()
        testData.id = testEntityID
        addTestData(testData)

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst()
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertFalse(results.contains { $0.id == self.testEntityID })
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.delete(TestDBEntity.self, id: testEntityID)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_DeleteAll_ByType() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        addTestData(count: 3)
        
        combineRealm.deleteAll(TestDBEntity.self)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
    
    func test_DeleteAll_ByEntities() {
        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssert(results.isEmpty)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        addTestData(count: 10)

        combineRealm.getAll(TestDBEntity.self)
            .flatMap { entities in
                self.combineRealm.deleteAll(Array(entities))
            }
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_DeleteWithPredicate() {
        let entities = (0...2).map { number in TestDBEntity(id: "\(number)") }

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    XCTAssertFalse(results.contains { $0.id == "1" })
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.addAll(entities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.deleteWithPredicate(
            TestDBEntity.self,
            predicate: NSPredicate(format:  "id == %@", "1")
        )
        .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }

    func test_DeleteCurrentAndAddAll() {
        let firstEntities = (10...15).map { number in TestDBEntity(id: "\(number)") }
        let secondEntities = (16...20).map { number in TestDBEntity(id: "\(number)") }
        let secondEntityIDs = (16...20).map { number in "\(number)" }

        combineRealm.getAll(TestDBEntity.self)
            .dropFirst(2)
            .prefix(1)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { results in
                    let ids = Array(results).map { $0.id }.sorted()
                    XCTAssertEqual(ids, secondEntityIDs)
                    self.expectation.fulfill()
                }
            )
            .store(in: &disposeBag)

        combineRealm.addAll(firstEntities)
            .sinkAndStore(in: &disposeBag)

        combineRealm.deleteCurrentAndAddAll(secondEntities)
            .sinkAndStore(in: &disposeBag)

        wait(for: [expectation], timeout: 1)
    }
}
