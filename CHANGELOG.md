## 1.0.0 Release notes (11-11-2022)

### Breaking Changes
- Package init
- Added a reactive Realm wrapper using Combine


## x.x.x Release notes (dd-mm-yyyy)

### Breaking Changes
### Deprecated
### Enhancements
### Bugfixes
